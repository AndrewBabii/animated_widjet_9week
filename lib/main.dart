import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  Duration _duration = Duration(seconds: 2);

  double _opacity = 1.0;
  double _height = 75.0;
  double _left = 50.0;
  double _padding = 8;
  Color _curColor = Colors.pinkAccent;

  void _incrementCounter(Color color) {
    setState(() {
      _padding += 50;
      _opacity = 0.0;
      _height += 75;
      _left += 100;
      _counter++;
_curColor = color;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: [
          DragTarget<Color>(
            onWillAccept: (val) => val != Colors.greenAccent,
            onAccept: (color) => _incrementCounter(color),
            builder: (context, candidates, rejects){
              return Container(
                height: 50,
                width: 50,
                color: _curColor,
              );
            },
          ),
          Container(
            height: 250,
            width: 300,
            child: Stack(
              children: <Widget>[
                AnimatedOpacity(
                  duration: _duration,
                  opacity: _opacity,
                  child: Text(
                    'You have pushed the button this many times:',
                  ),
                ),
                AnimatedPositioned(
                  duration: _duration,
                  left: _left,
                  top: 30,
                  child: AnimatedContainer(
                    color: Colors.pinkAccent,
                    height: _height,
                    duration: _duration,
                    child: Text(
                      '$_counter',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Draggable<Color>(
            feedback: Container(
              height: 50,
              width: 50,
              color: Colors.black,
            ),
            data: Colors.orangeAccent,
            child: Container(
              height: 50,
              width: 50,
              color: Colors.deepPurpleAccent,
            ),
            childWhenDragging:Container(
              height: 50,
              width: 50,
              color: Colors.grey,
            ),
          ),
        ],
      ),
      floatingActionButton: AnimatedPadding(
        padding: EdgeInsets.all(_padding),
        duration: _duration,
        /*child: FloatingActionButton(
          onPressed: _incrementCounter,
          tooltip: 'Increment',
          child: Icon(Icons.add),
        ),*/
      ),
    );
  }

}
